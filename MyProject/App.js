import * as React from "react";
import { Text, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import NameScreen from "./src/screen/name";
import PhoneScreen from "./src/screen/phone";
import EmailScreen from "./src/screen/email";
import HobbieScreen from "./src/screen/hobbie";
import Icon from "react-native-vector-icons/MaterialIcons";
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';

const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen
          name="Name"
          component={NameScreen}
          options={{
            tabBarIcon: ({ tintColor }) => (
              <FontAwesome5Icon name="user" size={23} color={tintColor} />
            )
          }}
        />

        <Tab.Screen
          name="Phone"
          component={PhoneScreen}
          options={{
            tabBarIcon: ({ tintColor }) => (
              <FontAwesome5Icon name="phone" size={22} color={tintColor} />
            )
          }}
        />

        <Tab.Screen
          name="Email"
          component={EmailScreen}
          options={{
            tabBarIcon: ({ tintColor }) => (
              <Icon name="email" size={25} color={tintColor} />
            )
          }}
        />

        <Tab.Screen
          name="Hobbie"
          component={HobbieScreen}
          options={{
            tabBarIcon: ({ tintColor }) => (
              <FontAwesome5Icon name="grin-stars" size={24} color={tintColor} />
            )
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}