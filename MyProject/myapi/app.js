// import express from 'express';
// import db from './db';
 
const express = require("express");
const db = require("./db");
 
const students = [
 {
 id: 1,
 name: "John",
 email: "john@abc.com"
 }
];
// setup express
const app = express();
 
app.get('/hello',(req,res) => {
 res.send("Wattanakran Buaphun!");
});
 
app.get('/api/v1/students', (req, res) => {
 console.log(students);
 res.status(200).send({
 success: 'true',
 message: 'students retrieved successfully',
 stds: students
 })
 });
// 0 - 65535 0-2^16
// 0 - 1023 : 25 (SMTP), 80(HTTP), 443(HTTPS)
// 3306 - MySQL, 
// netstat -an
 
const PORT = 5555;
app.listen(PORT,()=>{
 console.log(`server running on port ${PORT}`);
});