const functions = require('firebase-functions');
 
const express = require('express') // เรียกใช้ Express
const cors = require('cors');
const app = express() // สร้าง Object เก็บไว้ในตัวแปร app เพื่อนำไปใช้งาน
// Select Data
// Automatically allow cross-origin requests
app.use(cors({ origin: true }));
 
const students = [{
 "id": 1,
 "name": "Julieta",
 "email": "jyegorovnin0@ebay.co.uk"
 }, {
 "id": 2,
 "name": "Sophronia",
 "email": "shughes1@prnewswire.com"
 }, {
 "id": 3,
 "name": "Doloritas",
 "email": "drotherforth2@mashable.com"
 }, {
 "id": 4,
 "name": "Godiva",
 "email": "gskitterel3@google.com.au"
 }, {
 "id": 5,
 "name": "Birgit",
 "email": "bnicol4@facebook.com"
 }];
 
app.get('/api/v1/students', (req, res) => {
 console.log(students);
 res.json(students);
 });
exports.myapis = functions.https.onRequest(app);
exports.helloWorld = functions.https.onRequest((request, response) => {
 response.send("Hello from Vittayasak!");
});