import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import FontAwesome5Icon from 'react-native-vector-icons/Entypo';

export default function PhoneScreen() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text><FontAwesome5Icon name="phone" size={30} color="#0B6623" /></Text>
        <Text>0956095225!</Text>
      </View>
    );
}