import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import FontAwesome5Icon from 'react-native-vector-icons/AntDesign';
export default function HobbieScreen() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text><FontAwesome5Icon name="clockcircleo" size={30} color="#0B6623" /></Text>
        <Text>Basketball</Text>
      </View>
    );
  }