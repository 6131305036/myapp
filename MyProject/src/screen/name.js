import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import FontAwesome5Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default function NameScreen() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text><FontAwesome5Icon name="rename-box" size={40} color="#0B6623" /></Text>
      <Text>Wattanakran Buaphun !!</Text>
    </View>
  );
}