import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Entypo';

export default function EmailScreen() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text><Icon name="email" size={30} color="#0B6623" /></Text>
        <Text>6131305036@lamduan.ac.th!</Text>
      </View>
    );
}